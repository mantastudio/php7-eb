FROM php:7.3-apache

COPY config/custom.ini /usr/local/etc/php/conf.d/
COPY config/vhost.conf /etc/apache2/sites-enabled/
COPY config/application.ini /etc/php.d/

RUN apt-get update && apt-get install -y apt-utils zlib1g-dev libicu-dev libpq-dev libpng-dev libxml2-dev \
    && docker-php-ext-install opcache \
    && docker-php-ext-install fileinfo \
    && docker-php-ext-install gd \
    && docker-php-ext-install intl \
    && docker-php-ext-install xmlrpc \
    && docker-php-ext-install pdo_mysql \
    && pecl install apcu-5.1.16 \
    && docker-php-ext-enable apcu \
    && cp /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/

VOLUME /var/www/html
